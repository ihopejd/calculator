package com.example.calculator;

import android.text.TextUtils;
import android.widget.EditText;

public class ErrorAlert {
    public EditText oneEditText;
    public EditText secondEditText;

    ErrorAlert(EditText oneEditText, EditText twoEditText){
        this.oneEditText = oneEditText;
        this.secondEditText = twoEditText;
    }

    public boolean InputFieldIsEmpty(EditText editText){
        if(TextUtils.isEmpty(editText.getText())){
            editText.setError("Пустое Поле!");
            return true;
        }

        return false;
    }
}