package com.example.calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    private EditText oneEditText;
    private EditText secondEditText;
    private int firstNumber;
    private int secondNumber;
    private int valueResult;
    private String valueRes;
    private final String TAG = "Message";
    private final String TAG2 = "secondMessage";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        oneEditText = findViewById(R.id.first_number);
        secondEditText = findViewById(R.id.second_number);
    }


    private void parseIntValues(){
        firstNumber = Integer.parseInt(oneEditText.getText().toString());
        secondNumber = Integer.parseInt(secondEditText.getText().toString());

    }

    public void addition(View view){
        if (errorInputField())
            return;
        parseIntValues();
        valueResult = firstNumber + secondNumber;
        String operation = "+";
        newActivityWithSolution(operation);
    }

    public void subtraction(View view){
        if (errorInputField())
            return;
        parseIntValues();
        valueResult = firstNumber - secondNumber;
        String operation = "-";
        newActivityWithSolution(operation);
    }

    public void multiplication(View view){
        if (errorInputField())
            return;
        parseIntValues();
        valueResult = firstNumber * secondNumber;
        String operation = "*";
        newActivityWithSolution(operation);
    }

    public void division(View view){
        if (errorInputField())
            return;
        parseIntValues();
        valueResult = firstNumber / secondNumber;
        String operation = "/";
        newActivityWithSolution(operation);
    }

    private void  newActivityWithSolution(String operation){
        Intent intent = new Intent(this,SecondActivity.class);
        valueRes = String.valueOf(valueResult);
        intent.putExtra(TAG, oneEditText.getText().toString());
        intent.putExtra(TAG2, secondEditText.getText().toString());
        intent.putExtra("Operation", operation);
        intent.putExtra("Result", valueRes);
        startActivity(intent);
    }

    private boolean errorInputField() {
        ErrorAlert errorEditText = new ErrorAlert(oneEditText, secondEditText);
        if(errorEditText.InputFieldIsEmpty(oneEditText) | errorEditText.InputFieldIsEmpty(secondEditText)){
            return true;
        }
        return false;
    }
}


